export function onClickOutside(
  element: Ref<Element | undefined>,
  cb: () => void
) {
  const eventHandler = (event: globalThis.MouseEvent) => {
    if (element.value) {
      const { top, bottom, left, right } = element.value.getBoundingClientRect()
      const { clientX, clientY } = event

      if (
        clientX < left ||
        clientX > right ||
        clientY < top ||
        clientY > bottom
      ) {
        cb()
      }
    }
  }

  globalThis.document?.addEventListener('click', eventHandler)

  return () => globalThis.document?.removeEventListener('click', eventHandler)
}
