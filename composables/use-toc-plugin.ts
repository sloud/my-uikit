import {
  FormKitNode,
  FormKitSchemaNode,
  FormKitSchemaCondition,
} from '@formkit/core'

type Section = {
  toc: string
  id?: string
  label?: string
  node?: FormKitNode
  children: Section[]
}

type Toc = {
  sections: Section[]
}

function asArray<A>(a?: A | A[]): A[] {
  if (a === undefined) {
    return []
  }

  if (Array.isArray(a)) {
    return a
  }

  return [a]
}

function modifySchema(
  node: FormKitNode,
  modify: (schema: FormKitSchemaNode[]) => FormKitSchemaNode[]
) {
  if (!node.props.definition?.schema) return

  const { definition } = node.props
  const initialSchema = definition.schema

  if (typeof initialSchema === 'function') {
    definition.schema = (
      extensions: Record<
        string,
        Partial<FormKitSchemaNode> | FormKitSchemaCondition
      >
    ) => {
      const tempSchema = initialSchema(extensions)

      return modify(tempSchema)
    }
  } else if (Array.isArray(initialSchema)) {
    definition.schema = modify(initialSchema)
  } else if (initialSchema?.if) {
    definition.schema = {
      if: initialSchema.if,
      then: modify(asArray(initialSchema.then)),
      else: modify(asArray(initialSchema.else)),
    }
  }
}

function toSection(node: FormKitNode): Section | undefined {
  const toc = node.props.attrs?.toc
  const label = node.context?.label

  if (typeof toc === 'string') {
    return { toc, label, node, children: [] }
  }

  return undefined
}

function sectionPath(node: FormKitNode): string[] {
  const path: string[] = []
  let parent = node.parent
  let depth = 0

  while (parent) {
    const parentSection = toSection(parent)

    if (parentSection) {
      depth += 1
      path.unshift(parentSection.toc)
    }

    parent = parent.parent
  }

  return path
}

function insertSection(section: Section, path: string[], tree: Section[]) {
  if (path.length === 0) {
    tree.push(section)
  } else {
    const [key, ...rest] = path
    const parent = tree.find((it) => it.toc === key)

    if (parent) {
      insertSection(section, rest, parent.children)
    } else {
      const children: Section[] = []
      tree.push({ toc: key, children })
      insertSection(section, rest, children)
    }
  }
}

function removeSection(section: Section, path: string[], tree: Section[]) {
  if (path.length === 0) {
    const index = tree.findIndex((it) => it.toc === section.toc)
    tree.splice(index, 1)
  } else {
    const [key, ...rest] = path
    const parent = tree.find((it) => it.toc === key)

    if (parent) {
      removeSection(section, rest, parent.children)
    }
  }
}

export function useTocPlugin(id?: string) {
  const toc: Toc = reactive({ sections: [] })
  const plugin = (node: FormKitNode) => {
    node.on('created', ({ payload }: { payload: FormKitNode }) => {
      const section = toSection(payload)

      if (section) {
        const path = sectionPath(payload)

        section.id = ['toc', id, ...path, section.toc].join(':')
        insertSection(section, path, toc.sections)

        modifySchema(payload, (schema) => [
          {
            $el: 'div',
            attrs: {
              id: section.id,
            },
            children: [...schema],
          },
        ])
      }
    })

    node.on('destroying', ({ payload }: { payload: FormKitNode }) => {
      const section = toSection(payload)

      if (section) {
        const path = sectionPath(payload)
        console.log({
          msg: 'destroying',
          toc: section.toc,
          path: [...path, section.toc],
        })

        removeSection(section, path, toc.sections)
      }
    })
  }

  return { toc, plugin }
}
