const __toasts: Record<string, Toaster> = {}

export type ToastKind = 'info' | 'warn' | 'error'

export type Toaster = {
  nextId: number
  messages: Array<{
    id: number
    kind: ToastKind
    body: string
    holdOpen: boolean
  }>
}

export type ToastOptions = {
  body: string
  ttl?: number
}

function getToaster(id: string): Toaster {
  let toaster = __toasts[id]

  if (!toaster) {
    toaster = reactive({ nextId: 0, messages: [] })
    __toasts[id] = toaster
  }

  return toaster
}

function setAutoClose(id: number, toaster: Toaster, ttl: number = 5000) {
  if (Number.isFinite(ttl)) {
    globalThis.window?.setTimeout(() => {
      const message = toaster.messages.find((it) => it.id === id)

      if (message?.holdOpen) {
        setAutoClose(id, toaster, ttl)
      } else {
        toaster.messages = toaster.messages.filter(
          (message) => message.id !== id
        )
      }
    }, ttl)
  }
}

function pushToastMessage(
  kind: ToastKind,
  toaster: Toaster,
  options: ToastOptions
) {
  const id = toaster.nextId

  toaster.nextId += 1
  toaster.messages = [
    {
      id,
      kind,
      body: options?.body,
      holdOpen: false,
    },
    ...toaster.messages,
  ]

  setAutoClose(id, toaster, options?.ttl)
}

export const useToast = (id = 'toast') => {
  const toaster = getToaster(id)

  return {
    messages: computed(() => toaster.messages),

    info(body: string, ttl?: number) {
      pushToastMessage('info', toaster, { body, ttl })
    },

    warn(body: string, ttl?: number) {
      pushToastMessage('warn', toaster, { body, ttl })
    },

    error(body: string, ttl?: number) {
      pushToastMessage('error', toaster, { body, ttl })
    },

    dismiss(id: number) {
      toaster.messages = toaster.messages.filter((message) => message.id !== id)
    },

    hold(id: number, open = true) {
      for (const message of toaster.messages) {
        if (message.id === id) {
          message.holdOpen = open
        }
      }
    },
  }
}
