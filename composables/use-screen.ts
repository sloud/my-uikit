let __resizeTimeout: number
let __resizeListener = false
let __scrollListener = false
let __scrollThrottled = false
let __moveListeners = false
let __moveThrottled = false

const screen = reactive({
  width: 0,
  height: 0,
  scrollX: 0,
  scrollY: 0,
  cursorX: 0,
  cursorY: 0,
})

function onResize() {
  screen.width =
    globalThis.window?.innerWidth ??
    globalThis.document?.documentElement.clientWidth ??
    globalThis.document?.body?.clientWidth ??
    0

  screen.height =
    globalThis.window?.innerHeight ??
    globalThis.document?.documentElement.clientHeight ??
    globalThis.document?.body.clientHeight ??
    0
}

function setResizeListener() {
  // we only want a single global resize listener created no matter how many times
  // `useScreen()` is called.
  if (!__resizeListener) {
    globalThis.window?.addEventListener('resize', () => {
      globalThis.window.clearTimeout(__resizeTimeout)
      __resizeTimeout = globalThis.window.setTimeout(onResize, 60)
    })

    onResize()
    __resizeListener = true
  }
}

function onScroll() {
  screen.scrollX =
    globalThis.window?.scrollX ?? globalThis.window?.pageXOffset ?? 0
  screen.scrollY =
    globalThis.window?.scrollY ?? globalThis.window?.pageYOffset ?? 0

  // unblock the guard so another timeout can be set
  __scrollThrottled = false
}

function setScrollListener() {
  // we only want a single global scroll listener created no matter how many times
  // `useScreen()` is called.
  if (!__scrollListener) {
    globalThis.window?.addEventListener('scroll', () => {
      if (!__scrollThrottled) {
        globalThis.window?.setTimeout(onScroll, 200)

        // block the callback so more timeouts are not scheduled
        __scrollThrottled = true
      }
    })

    onScroll()
    __scrollListener = true
  }
}

function onMouseMove(event: globalThis.MouseEvent) {
  screen.cursorX = event?.clientX ?? 0
  screen.cursorY = event?.clientY ?? 0

  // unblock the guard so another timeout can be set
  __moveThrottled = false
}

function onTouchMove(event: globalThis.TouchEvent) {
  screen.cursorX = event?.touches[0]?.clientX ?? 0
  screen.cursorY = event?.touches[0]?.clientY ?? 0

  // unblock the guard so another timeout can be set
  __moveThrottled = false
}

function setMoveListeners() {
  // we only want a single global mousemove listener created no matter how many times
  // `useScreen()` is called.
  if (!__moveListeners) {
    globalThis.window?.addEventListener('mousemove', (event) => {
      if (!__moveThrottled) {
        globalThis.window?.setTimeout(() => onMouseMove(event), 200)

        // block the callback so more timeouts are not scheduled
        __moveThrottled = true
      }
    })

    globalThis.window?.addEventListener('touchmove', (event) => {
      if (!__moveThrottled) {
        globalThis.window?.setTimeout(() => onTouchMove(event), 200)

        // block the callback so more timeouts are not scheduled
        __moveThrottled = true
      }
    })

    __moveListeners = true
  }
}

export const useScreen = () => {
  setResizeListener()
  setScrollListener()
  setMoveListeners()

  return {
    screenWidth: computed(() => screen.width),
    screenHeight: computed(() => screen.height),
    scrollX: computed(() => screen.scrollX),
    scrollY: computed(() => screen.scrollY),
    cursorX: computed(() => screen.cursorX),
    cursorY: computed(() => screen.cursorY),
    pageX: computed(() => screen.cursorX + screen.scrollX),
    pageY: computed(() => screen.cursorY + screen.scrollY),
  }
}
