const __modals: Record<string, Ref<HTMLDialogElement | undefined>> = {}

function open(id: string, options: { modal: boolean }) {
  setTimeout(() => {
    if (options.modal) {
      __modals[id]?.value?.showModal()
    } else {
      __modals[id]?.value?.show()
    }
  }, 10)
}

function close(id: string) {
  setTimeout(() => {
    __modals[id]?.value?.close()
  }, 10)
}

function toggle(id: string, options: { modal: boolean }) {
  __modals[id]?.value?.open ? close(id) : open(id, options)
}

export const useModal = (id: string) => {
  if (__modals[id] === undefined) {
    __modals[id] = ref()
  }

  return {
    modal: __modals[id],

    open(options: { modal: boolean } = { modal: true }) {
      open(id, options)
    },

    close() {
      close(id)
    },

    toggle(options: { modal: boolean } = { modal: true }) {
      toggle(id, options)
    },
  }
}
